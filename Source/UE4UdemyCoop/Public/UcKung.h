// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <set>
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UcKung.generated.h"

class USkeletalMeshComponent;
class UParticleSystemComponent;
class UDamageType;
class AUcCharacter;
class AUcImmortalFire;

UCLASS()
class UE4UDEMYCOOP_API AUcKung : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUcKung();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	USkeletalMeshComponent* m_pSkMeshComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UParticleSystemComponent* m_pEffectComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Kung)
	TSubclassOf<UDamageType> m_cDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Kung)
	float m_fFleshDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Kung)
	float m_fIronCoverDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	UParticleSystem* m_pLineEffect;//FLAGJK 粒子本身还需再调整（发光效果相对位置改绝对，时间调短）

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	UParticleSystem* m_pFleshImpEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	UParticleSystem* m_pIronCoverImpEffect;

	//TODOJK 蓝图重写
	virtual void ReloadFireSubClass();

	void SpawnFire(const FVector& vLoc, const FVector& vDir);
	void TryDelTimer(FTimerHandle* pTimer);

	std::list<FTimerHandle*> m_lisTimers;
	std::set<AUcImmortalFire*> m_setFires;

	UClass* m_pcFire;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//UFUNCTION(BlueprintCallable, BlueprintProtected, Category = Kung)
	void ImmortalFire();
	void TryDelFire(AUcImmortalFire* pFire);
};
