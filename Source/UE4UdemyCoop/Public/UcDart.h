// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UcDart.generated.h"

class UStaticMeshComponent;
class USphereComponent;
class UProjectileMovementComponent;

UCLASS()
class UE4UDEMYCOOP_API AUcDart : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUcDart();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UStaticMeshComponent* m_pMeshComp;

	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	//USphereComponent* m_pSphereComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UProjectileMovementComponent* m_pProjMovComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	UParticleSystem* m_pBoomEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Kung)
	TSubclassOf<UDamageType> m_cDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Kung)
	TSubclassOf<UCameraShake> m_cCamShake;

	UFUNCTION()
	void OnSphereOverlapped(UPrimitiveComponent* pOverlappedComp, AActor* pOtherActor, UPrimitiveComponent* pOtherComp,
		int32 iOtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnMeshHit(UPrimitiveComponent* pHitComp, AActor* pOtherActor, UPrimitiveComponent* pOtherComp,
		FVector vNormalImpulse, const FHitResult& Hit);

	void Boom();

	bool m_bDeepThick;
	float m_fSOrT;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SetDeepThick(bool b);

	FVector m_vDir;
};
