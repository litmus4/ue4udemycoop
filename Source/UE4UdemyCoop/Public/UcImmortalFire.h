// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UcImmortalFire.generated.h"

class UParticleSystemComponent;
class USphereComponent;

UCLASS()
class UE4UDEMYCOOP_API AUcImmortalFire : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUcImmortalFire();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UParticleSystemComponent* m_pEffectComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	USphereComponent* m_pSphereComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Effects)
	UParticleSystem* m_pBoomEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Kung)
	TSubclassOf<UDamageType> m_cDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Kung)
	TSubclassOf<UCameraShake> m_cCamShake;

	UFUNCTION()
	void OnSphereOverlapped(UPrimitiveComponent* pOverlappedComp, AActor* pOtherActor, UPrimitiveComponent* pOtherComp,
		int32 iOtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	float m_fS;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	FVector m_vDir;
};
