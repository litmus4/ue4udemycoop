// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UcKung.h"
#include "UcFaWithKung.generated.h"

/**
 * 
 */
UCLASS()
class UE4UDEMYCOOP_API AUcFaWithKung : public AUcKung
{
	GENERATED_BODY()

public:
	AUcFaWithKung();
	
public:
	//TODOJK ��ͼ��д
	virtual void Attack(bool bDeepThick);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Fa)
	TSubclassOf<AActor> m_cDart;

	void SpawnDart(const FVector& vLoc, const FVector& vDir, bool bDeepThick);
};
