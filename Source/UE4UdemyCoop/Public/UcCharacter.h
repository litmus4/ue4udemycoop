// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "UcCharacter.generated.h"

class UCameraComponent;
class USpringArmComponent;
class AUcKung;

UCLASS()
class UE4UDEMYCOOP_API AUcCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AUcCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveForward(float fValue);
	void MoveRight(float fValue);
	void BeginCrouch();
	void EndCrouch();
	void JumpEx();
	void OnFire();
	void OnNormalAttack();
	void OnDeepAttack();
	void BeginZoom();
	void EndZoom();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UCameraComponent* m_pCamComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	USpringArmComponent* m_pSpArmComp;

	AUcKung* m_pKung;

	bool m_bZoom;
	float m_fCurZoomTime;
	float m_fSrcFov;
	float m_fFovDefault;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Player)
	float m_fFovZoom;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Player)
	float m_fZoomTime;

	UClass* m_pcAnimInst;
	UClass* m_pcKung;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
