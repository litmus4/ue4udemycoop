// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include <map>
#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UE4UdemyCoopGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UE4UDEMYCOOP_API AUE4UdemyCoopGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AUE4UdemyCoopGameModeBase();

	unsigned int GenIndex(const std::string& strKey);

private:
	std::map<std::string, unsigned int> m_mapIndex;
};
