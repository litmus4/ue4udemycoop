// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.


#include "UE4UdemyCoopGameModeBase.h"
#include "Public/UcCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUE4UdemyCoopGameModeBase::AUE4UdemyCoopGameModeBase()
{
	static ConstructorHelpers::FObjectFinder<UBlueprint> subFinder(TEXT("Blueprint'/Game/Blueprints/BP_Player.BP_Player'"));
	if (subFinder.Object)
		DefaultPawnClass = subFinder.Object->GeneratedClass;
}

unsigned int AUE4UdemyCoopGameModeBase::GenIndex(const std::string& strKey)
{
	std::map<std::string, unsigned int>::iterator iter = m_mapIndex.find(strKey);
	if (iter != m_mapIndex.end())
		return iter->second++;
	m_mapIndex.insert(std::make_pair(strKey, (unsigned int)1));
	return 0;
}