// Fill out your copyright notice in the Description page of Project Settings.


#include "UcFaWithKung.h"
#include "UcCharacter.h"
#include "UcDart.h"
#include "../UE4UdemyCoopGameModeBase.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"

AUcFaWithKung::AUcFaWithKung()
{
	//
}

void AUcFaWithKung::Attack(bool bDeepThick)
{
	ACharacter* pOwner = Cast<ACharacter>(GetOwner());
	if (!pOwner) return;

	FVector vStart, vDir, vEnd;
	bool bDeproj = false;
	APlayerController* pController = pOwner->GetController<APlayerController>();
	if (pController)
	{
		FVector2D v2Size = GEngine->GameViewport->Viewport->GetSizeXY();
		if (pController->DeprojectScreenPositionToWorld(v2Size.X / 2, v2Size.Y / 2, vStart, vDir))
			bDeproj = true;
	}
	if (!bDeproj)
	{
		FRotator rEye;
		pOwner->GetActorEyesViewPoint(vStart, rEye);
		vDir = rEye.Vector();
	}
	vEnd = vStart + vDir * 10000;

	FHitResult hit;
	FCollisionQueryParams param;
	param.AddIgnoredActor(pOwner);
	param.AddIgnoredActor(this);
	param.bTraceComplex = true;//trueͼԪ false��Χ��
	FVector* pvHit = nullptr;
	if (GetWorld()->LineTraceSingleByChannel(hit, vStart, vEnd, ECC_GameTraceChannel1, param))
	{
		AActor* pHitActor = hit.GetActor();
		pvHit = new FVector(hit.ImpactPoint);
	}

	FTimerHandle* pTimer = new FTimerHandle();
	FTimerDelegate timerDelegate;
	timerDelegate.BindLambda([=]() {
		FVector vHand = pOwner->GetMesh()->GetSocketLocation(TEXT("HandSocket"));
		if (pvHit)
		{
			*pvHit -= vHand;
			pvHit->Normalize();
			SpawnDart(vHand, *pvHit, bDeepThick);
			delete pvHit;
		}
		else
			SpawnDart(vHand, pOwner->GetActorRotation().Vector(), bDeepThick);
		TryDelTimer(pTimer);
	});
	GetWorldTimerManager().SetTimer(*pTimer, timerDelegate, 1.0f, false);
	m_lisTimers.push_back(pTimer);
}

void AUcFaWithKung::SpawnDart(const FVector& vLoc, const FVector& vDir, bool bDeepThick)
{
	AUE4UdemyCoopGameModeBase* pGM = Cast<AUE4UdemyCoopGameModeBase>(GetWorld()->GetAuthGameMode());
	if (!pGM) return;

	std::string strName = "Dart";
	char szBuf[32];
	strName += itoa(pGM->GenIndex(strName), szBuf, 10);

	FTransform trans(FRotationMatrix::MakeFromX(vDir).Rotator(), vLoc);
	FActorSpawnParameters param;
	param.Name = FName(strName.c_str());
	param.Owner = this;
	param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	AUcDart* pDart = Cast<AUcDart>(GetWorld()->SpawnActor((UClass*)m_cDart, &trans, param));
	if (pDart)
	{
		pDart->SetDeepThick(bDeepThick);
		pDart->m_vDir = vDir;
	}
}