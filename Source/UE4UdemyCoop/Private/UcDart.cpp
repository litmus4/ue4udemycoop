// Fill out your copyright notice in the Description page of Project Settings.


#include "UcDart.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

// Sets default values
AUcDart::AUcDart()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_pMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	m_pMeshComp->SetSimulatePhysics(true);
	m_pMeshComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	m_pMeshComp->SetCollisionResponseToAllChannels(ECR_Block);
	m_pMeshComp->OnComponentHit.AddDynamic(this, &AUcDart::OnMeshHit);
	RootComponent = m_pMeshComp;

	//m_pSphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	//m_pSphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	//m_pSphereComp->SetCollisionResponseToAllChannels(ECR_Overlap);
	//m_pSphereComp->SetupAttachment(m_pMeshComp);
	//m_pSphereComp->OnComponentBeginOverlap.AddDynamic(this, &AUcDart::OnSphereOverlapped);

	m_pProjMovComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjMovComp"));
	m_pProjMovComp->InitialSpeed = 2000.0f;
	m_pProjMovComp->MaxSpeed = 2000.0f;
	m_pProjMovComp->bShouldBounce = true;

	m_bDeepThick = false;
	m_fSOrT = 0.0f;
}

// Called when the game starts or when spawned
void AUcDart::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AUcDart::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (m_bDeepThick)
	{
		float fDeltaS = DeltaTime * 3000;
		SetActorLocation(GetActorLocation() + m_vDir * fDeltaS);
		m_fSOrT += fDeltaS;
		if (m_fSOrT > 10000.0f)
			Destroy();
	}
	else
	{
		m_fSOrT += DeltaTime;
		if (m_fSOrT >= 1.0f)
			Boom();
	}
}

void AUcDart::OnSphereOverlapped(UPrimitiveComponent* pOverlappedComp, AActor* pOtherActor, UPrimitiveComponent* pOtherComp,
	int32 iOtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (m_bDeepThick)
	{
		AActor* pOwner = GetOwner();
		if (!pOwner || pOtherActor == pOwner->GetOwner()) return;
		Boom();
	}
}

void AUcDart::OnMeshHit(UPrimitiveComponent* pHitComp, AActor* pOtherActor, UPrimitiveComponent* pOtherComp,
	FVector vNormalImpulse, const FHitResult& Hit)
{
	//FLAGJK 为什么无法触发overlap或hit？下边函数已经调整mesh的碰撞参数了啊？
	//*测试临时
	UE_LOG(LogTemp, Log, TEXT("$$$$$$$$$$$$$$$$$$$$$$$$"));
	//*/
	if (m_bDeepThick)
	{
		AActor* pOwner = GetOwner();
		if (!pOwner || pOtherActor == pOwner->GetOwner()) return;
		Boom();
	}
}

void AUcDart::SetDeepThick(bool b)
{
	m_bDeepThick = b;
	//m_pMeshComp->SetSimulatePhysics(!b);
	//m_pMeshComp->SetCollisionEnabled(b ? ECollisionEnabled::NoCollision : ECollisionEnabled::QueryAndPhysics);
	//m_pMeshComp->SetCollisionResponseToAllChannels(b ? ECR_Ignore : ECR_Block);
	m_pMeshComp->SetEnableGravity(!b);
	m_pProjMovComp->SetActive(!b);//EXP ProjectileMovementComponent激活时不能SetActorLocation
}

void AUcDart::Boom()
{
	AActor* pOwner = GetOwner();
	if (!pOwner) return;

	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), m_pBoomEffect, GetActorLocation());
	APawn* pPlayer = Cast<APawn>(pOwner->GetOwner());
	if (pPlayer)
	{
		APlayerController* pController = pPlayer->GetController<APlayerController>();
		if (pController && FVector::Distance(GetActorLocation(), pPlayer->GetActorLocation()) < 1000.0f)
			pController->ClientPlayCameraShake(m_cCamShake);
	}
	DrawDebugSphere(GetWorld(), GetActorLocation(), 250.0f, 12, FColor::Red, false, 0.3f);

	TArray<AActor*> arrIgActors;
	if (pPlayer)
		arrIgActors.Add(pPlayer);
	UGameplayStatics::ApplyRadialDamage(GetWorld(), 100.0f, GetActorLocation(), 250.0f,
		m_cDamage, arrIgActors, pOwner, pOwner->GetInstigatorController());

	Destroy();
}