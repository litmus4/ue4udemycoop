// Fill out your copyright notice in the Description page of Project Settings.


#include "UcKung.h"
#include "UcCharacter.h"
#include "UcImmortalFire.h"
#include "../UE4UdemyCoopGameModeBase.h"
#include "Components/SkeletalMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TimerManager.h"
#include "DrawDebugHelpers.h"

int32 iDbgKungFire = 0;
FAutoConsoleVariableRef cvr(TEXT("COOP.DebugKungFire"), iDbgKungFire, TEXT("Draw debug line."), ECVF_Cheat);

// Sets default values
AUcKung::AUcKung()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_pSkMeshComp = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkMeshComp"));
	RootComponent = m_pSkMeshComp;

	m_pEffectComp = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("EffectComp"));
	m_pEffectComp->SetupAttachment(m_pSkMeshComp);

	static ConstructorHelpers::FObjectFinder<UBlueprint> subFinder(TEXT("Blueprint'/Game/Blueprints/BP_ImmortalFire.BP_ImmortalFire'"));
	if (subFinder.Object)
		m_pcFire = (UClass*)subFinder.Object->GeneratedClass;
}

// Called when the game starts or when spawned
void AUcKung::BeginPlay()
{
	Super::BeginPlay();
	
	ReloadFireSubClass();
}

// Called every frame
void AUcKung::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AUcKung::ImmortalFire()
{
	ACharacter* pOwner = Cast<ACharacter>(GetOwner());
	if (!pOwner) return;

	FVector vStart, vDir, vEnd;
	bool bDeproj = false;
	APlayerController* pController = pOwner->GetController<APlayerController>();
	if (pController)
	{
		FVector2D v2Size = GEngine->GameViewport->Viewport->GetSizeXY();
		if (pController->DeprojectScreenPositionToWorld(v2Size.X / 2, v2Size.Y / 2, vStart, vDir))
			bDeproj = true;
	}
	if (!bDeproj)
	{
		FRotator rEye;
		pOwner->GetActorEyesViewPoint(vStart, rEye);
		vDir = rEye.Vector();
	}
	vEnd = vStart + vDir * 10000;

	FHitResult hit;
	FCollisionQueryParams param;
	param.AddIgnoredActor(pOwner);
	param.AddIgnoredActor(this);
	param.bTraceComplex = true;//trueͼԪ false��Χ��
	param.bReturnPhysicalMaterial = true;
	FVector* pvHit = nullptr;
	if (GetWorld()->LineTraceSingleByChannel(hit, vStart, vEnd, ECC_GameTraceChannel1, param))
	{
		AActor* pHitActor = hit.GetActor();
		pvHit = new FVector(hit.ImpactPoint);

		EPhysicalSurface eSurfType = UPhysicalMaterial::DetermineSurfaceType(hit.PhysMaterial.Get());
		UParticleSystem* pImpactEffect = nullptr;
		float fDamage = 0.0f;
		switch (eSurfType)
		{
		case SurfaceType1:
			pImpactEffect = m_pFleshImpEffect;
			fDamage = m_fFleshDamage;
			break;
		case SurfaceType2:
			pImpactEffect = m_pIronCoverImpEffect;
			fDamage = m_fIronCoverDamage;
			break;
		}

		if (pImpactEffect)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), pImpactEffect,
				hit.ImpactPoint, hit.ImpactNormal.Rotation());
		}
		UGameplayStatics::ApplyPointDamage(pHitActor, fDamage, vDir, hit,
			pOwner->GetController(), this, m_cDamage);
	}

	if (iDbgKungFire > 0)
		DrawDebugLine(GetWorld(), vStart, vEnd, FColor::White, false, 0.3f, 0, 1.0f);
	UParticleSystemComponent* pEffectComp =
		UGameplayStatics::SpawnEmitterAttached(m_pLineEffect, pOwner->GetMesh(), TEXT("HandSocket"));
	if (pEffectComp)
	{
		FVector vLineTarget = vEnd;
		if (pvHit)
			vLineTarget = *pvHit;
		pEffectComp->SetVectorParameter(TEXT("beamEnd"), vLineTarget);
		pEffectComp->SetRelativeScale3D(FVector(2.0f));
	}

	FTimerHandle* pTimer = new FTimerHandle();
	FTimerDelegate timerDelegate;
	timerDelegate.BindLambda([=]() {
		FVector vHand = pOwner->GetMesh()->GetSocketLocation(TEXT("HandSocket"));
		if (pvHit)
		{
			*pvHit -= vHand;
			pvHit->Normalize();
			SpawnFire(vHand, *pvHit);
			delete pvHit;
		}
		else
			SpawnFire(vHand, pOwner->GetActorRotation().Vector());
		TryDelTimer(pTimer);
	});
	GetWorldTimerManager().SetTimer(*pTimer, timerDelegate, 1.0f, false);
	m_lisTimers.push_back(pTimer);
}

void AUcKung::SpawnFire(const FVector& vLoc, const FVector& vDir)
{
	AUE4UdemyCoopGameModeBase* pGM = Cast<AUE4UdemyCoopGameModeBase>(GetWorld()->GetAuthGameMode());
	if (!pGM) return;

	std::string strName = "ImmortalFire";
	char szBuf[32];
	strName += itoa(pGM->GenIndex(strName), szBuf, 10);

	FTransform trans(FRotationMatrix::MakeFromZ(-vDir).Rotator(), vLoc);
	FActorSpawnParameters param;
	param.Name = FName(strName.c_str());
	param.Owner = this;
	param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	AUcImmortalFire* pFire = Cast<AUcImmortalFire>(GetWorld()->SpawnActor(m_pcFire, &trans, param));
	if (pFire)
	{
		pFire->m_vDir = vDir;
		m_setFires.insert(pFire);
	}
}

void AUcKung::TryDelFire(AUcImmortalFire* pFire)
{
	std::set<AUcImmortalFire*>::iterator iter = m_setFires.find(pFire);
	if (iter != m_setFires.end())
	{
		(*iter)->Destroy();
		m_setFires.erase(iter);
	}
}

void AUcKung::TryDelTimer(FTimerHandle* pTimer)
{
	std::list<FTimerHandle*>::iterator iter = m_lisTimers.begin();
	for (; iter != m_lisTimers.end(); iter++)
	{
		if (**iter == *pTimer)
		{
			//if (GetWorldTimerManager().IsTimerActive(**iter))
			//	GetWorldTimerManager().ClearTimer(**iter);
			delete *iter;
			m_lisTimers.erase(iter);
			return;
		}
	}
}

void AUcKung::ReloadFireSubClass()
{
	//
}