// Fill out your copyright notice in the Description page of Project Settings.


#include "UcImmortalFire.h"
#include "UcKung.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

// Sets default values
AUcImmortalFire::AUcImmortalFire()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_pEffectComp = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("EffectComp"));
	m_pEffectComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RootComponent = m_pEffectComp;

	m_pSphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	m_pSphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	m_pSphereComp->SetCollisionResponseToAllChannels(ECR_Overlap);
	m_pSphereComp->SetupAttachment(m_pEffectComp);
	m_pSphereComp->OnComponentBeginOverlap.AddDynamic(this, &AUcImmortalFire::OnSphereOverlapped);

	m_fS = 0.0f;
}

// Called when the game starts or when spawned
void AUcImmortalFire::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AUcImmortalFire::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	float fDeltaS = DeltaTime * 3000;
	SetActorLocation(GetActorLocation() + m_vDir * fDeltaS);
	m_fS += fDeltaS;
	if (m_fS > 10000.0f)
	{
		AUcKung* pOwner = Cast<AUcKung>(GetOwner());
		if (pOwner)
			pOwner->TryDelFire(this);
	}
}

void AUcImmortalFire::OnSphereOverlapped(UPrimitiveComponent* pOverlappedComp, AActor* pOtherActor, UPrimitiveComponent* pOtherComp,
	int32 iOtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AUcKung* pOwner = Cast<AUcKung>(GetOwner());
	if (!pOwner || pOtherActor == pOwner->GetOwner()) return;

	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), m_pBoomEffect, GetActorLocation());
	APawn* pPlayer = Cast<APawn>(pOwner->GetOwner());
	if (pPlayer)
	{
		APlayerController* pController = pPlayer->GetController<APlayerController>();
		if (pController && FVector::Distance(GetActorLocation(), pPlayer->GetActorLocation()) < 1000.0f)
			pController->ClientPlayCameraShake(m_cCamShake);
	}
	DrawDebugSphere(GetWorld(), GetActorLocation(), 250.0f, 12, FColor::Red, false, 0.3f);

	TArray<AActor*> arrIgActors;
	if (pPlayer)
		arrIgActors.Add(pPlayer);
	UGameplayStatics::ApplyRadialDamage(GetWorld(), 100.0f, GetActorLocation(), 250.0f, m_cDamage, arrIgActors);
	//FLAGJK TargetDummy无法接收RadialDamage事件

	pOwner->TryDelFire(this);
}

