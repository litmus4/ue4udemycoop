// Fill out your copyright notice in the Description page of Project Settings.


#include "UcCharacter.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Animation/AnimInstance.h"
#include "UcFaWithKung.h"

// Sets default values
AUcCharacter::AUcCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_pSpArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpArmComp"));
	m_pSpArmComp->bUsePawnControlRotation = true;
	m_pSpArmComp->SetupAttachment(RootComponent);

	m_pCamComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CamComp"));
	m_pCamComp->SetupAttachment(m_pSpArmComp);

	//GetNavAgentPropertiesRef().bCanCrouch = true;//只能在蓝图中设置了
	static ConstructorHelpers::FObjectFinder<UBlueprint> subFinder(TEXT("AnimBlueprint'/Game/Blueprints/AnimBps/ABP_SsBoxing.ABP_SsBoxing'"));
	if (subFinder.Object)
		m_pcAnimInst = (UClass*)subFinder.Object->GeneratedClass;
	/*static ConstructorHelpers::FClassFinder<UAnimBlueprint> subFinder(TEXT("AnimBlueprint'/Game/Blueprints/AnimBps/ABP_SsBoxing.ABP_SsBoxing_C'"));
	m_pcAnimInst = (UClass*)subFinder.Class;*/
	//Blueprint'/Game/Blueprints/BP_Kung1.BP_Kung1' Blueprint'/Game/Blueprints/BP_Fa1.BP_Fa1'
	static ConstructorHelpers::FObjectFinder<UBlueprint> subFinder2(TEXT("Blueprint'/Game/Blueprints/BP_Fa1.BP_Fa1'"));
	if (subFinder2.Object)
		m_pcKung = (UClass*)subFinder2.Object->GeneratedClass;

	m_bZoom = false;
	m_fCurZoomTime = -1.0f;
	m_fFovZoom = 65.0f;
	m_fZoomTime = 0.2f;
}

// Called when the game starts or when spawned
void AUcCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	FActorSpawnParameters param;
	param.Name = TEXT("Kung");
	param.Owner = this;
	param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	FTransform trans;
	m_pKung = Cast<AUcKung>(GetWorld()->SpawnActor(m_pcKung, &trans, param));
	if (m_pKung)
		m_pKung->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, TEXT("KungSocket"));

	m_fFovDefault = m_pCamComp->FieldOfView;
	m_fSrcFov = m_pCamComp->FieldOfView;
}

void AUcCharacter::MoveForward(float fValue)
{
	AddMovementInput(GetActorForwardVector() * fValue);
}

void AUcCharacter::MoveRight(float fValue)
{
	AddMovementInput(GetActorRightVector() * fValue);
}

void AUcCharacter::BeginCrouch()
{
	Crouch();
}

void AUcCharacter::EndCrouch()
{
	UnCrouch();
}

void AUcCharacter::JumpEx()
{
	//Cast<m_pcAnimInst>GetMesh()->GetAnimInstance()//TODOJK 判断能否跳
	Jump();
}

void AUcCharacter::OnFire()
{
	if (!m_pKung) return;
	m_pKung->ImmortalFire();
}

void AUcCharacter::OnNormalAttack()
{
	AUcFaWithKung* pFa = Cast<AUcFaWithKung>(m_pKung);
	if (!pFa) return;
	pFa->Attack(false);
}

void AUcCharacter::OnDeepAttack()
{
	AUcFaWithKung* pFa = Cast<AUcFaWithKung>(m_pKung);
	if (!pFa) return;
	pFa->Attack(true);
}

void AUcCharacter::BeginZoom()
{
	if (m_bZoom) return;
	m_bZoom = true;
	m_fCurZoomTime = 0.0f;
	m_fSrcFov = m_pCamComp->FieldOfView;
}

void AUcCharacter::EndZoom()
{
	if (!m_bZoom) return;
	m_bZoom = false;
	m_fCurZoomTime = 0.0f;
	m_fSrcFov = m_pCamComp->FieldOfView;
}

// Called every frame
void AUcCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (m_fCurZoomTime >= -0.5f)
	{
		m_fCurZoomTime += DeltaTime;
		float fDstFov = (m_bZoom ? m_fFovZoom : m_fFovDefault);
		if (m_fCurZoomTime < m_fZoomTime)
		{
			float fCurFov = FMath::Lerp(m_fSrcFov, fDstFov, m_fCurZoomTime / m_fZoomTime);
			//FMath::FInterpTo()
			m_pCamComp->SetFieldOfView(fCurFov);
		}
		else
		{
			m_pCamComp->SetFieldOfView(fDstFov);
			m_fCurZoomTime = -1.0f;
		}
	}
}

// Called to bind functionality to input
void AUcCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AUcCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AUcCharacter::MoveRight);

	PlayerInputComponent->BindAxis("PitchDown", this, &AUcCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("YawRight", this, &AUcCharacter::AddControllerYawInput);

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &AUcCharacter::BeginCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &AUcCharacter::EndCrouch);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AUcCharacter::JumpEx);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AUcCharacter::OnFire);
	PlayerInputComponent->BindAction("NormalAttack", IE_Pressed, this, &AUcCharacter::OnNormalAttack);
	PlayerInputComponent->BindAction("DeepAttack", IE_Pressed, this, &AUcCharacter::OnDeepAttack);

	PlayerInputComponent->BindAction("Zoom", IE_Pressed, this, &AUcCharacter::BeginZoom);
	PlayerInputComponent->BindAction("Zoom", IE_Released, this, &AUcCharacter::EndZoom);
}

